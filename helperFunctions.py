from SPARQLWrapper import SPARQLWrapper, SmartWrapper, RDFXML, JSON, XML, TURTLE, JSONLD
from rdflib import Namespace
import logging
import sys
import time
from rdflib import URIRef, BNode, Literal, Graph
import math
import os
import pickle
import datetime as dt

logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                    format='%(asctime)-15s (%(threadName)-9s) %(message)s', )
logging.getLogger("requests").setLevel(logging.WARNING)

namespaces_used = [['wd', Namespace("http://www.wikidata.org/entity/")],
                   ['wds', Namespace("http://www.wikidata.org/entity/statement/")],
                   ['wdt', Namespace("http://www.wikidata.org/prop/direct/")],
                   ['wdv', Namespace("http://www.wikidata.org/value/")],
                   ['wdtn', Namespace("http://www.wikidata.org/prop/direct-normalized/")],
                   ['wdref', Namespace("http://www.wikidata.org/reference/")],
                   ['wdno', Namespace("http://www.wikidata.org/prop/novalue/")],
                   ['ps', Namespace("http://www.wikidata.org/prop/statement/")],
                   ['pq', Namespace("http://www.wikidata.org/prop/qualifier/")],
                   ['bd', Namespace("http://www.bigdata.com/rdf#")],
                   ['psv', Namespace("http://www.wikidata.org/prop/statement/value/")],
                   ['psn', Namespace("http://www.wikidata.org/prop/statement/value-normalized/")],
                   ['pqv', Namespace("http://www.wikidata.org/prop/qualifier/value/")],
                   ['pqn', Namespace("http://www.wikidata.org/prop/qualifier/value-normalized/")],
                   ['pr', Namespace("http://www.wikidata.org/prop/reference/")],
                   ['prv', Namespace("http://www.wikidata.org/prop/reference/value/")],
                   ['prn', Namespace("http://www.wikidata.org/prop/reference/value-normalized/")],
                   ['xsd', Namespace("http://www.w3.org/2001/XMLSchema#")],
                   ['ontolex', Namespace("http://www.w3.org/ns/lemon/ontolex#")],
                   ['dct', Namespace("http://purl.org/dc/terms/")],
                   ['owl', Namespace("http://www.w3.org/2002/07/owl#")],
                   ['skos', Namespace("http://www.w3.org/2004/02/skos/core#")],
                   ['cc', Namespace("http://creativecommons.org/ns#")],
                   ['geo', Namespace("http://www.opengis.net/ont/geosparql#")],
                   ['prov', Namespace("http://www.w3.org/ns/prov#")],
                   ['wdata', Namespace("http://www.wikidata.org/wiki/Special:EntityData/")],
                   ['bd', Namespace("http://www.bigdata.com/rdf#")],
                   ['hint', Namespace("http://www.bigdata.com/queryHints#")],
                   ['wikibase', Namespace("http://wikiba.se/ontology#")],
                   ['schema', Namespace("http://schema.org/")],
                   ['rdfs', Namespace("http://www.w3.org/2000/01/rdf-schema#")],
                   ['p', Namespace("http://www.wikidata.org/prop/")]]

class Query:

    def __init__(self):
        self.endpoint_url = "https://query.wikidata.org/sparql"
        self.user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])
        self.max_query = 450

    def get_results(self, query,
                    total_values,
                    timeout=60,
                    max_attempts=3,
                    bulk_values=None,
                    split_amount=2,
                    sleep_time=300,
                    format=RDFXML):
        """
        Performs Sparql query using self.endpoint_url
        :param query: wikidata query
        :param timeout: timeout for wikidata query, defaults to ten seconds
        :param bulk_values: a set of values, if true bulk query will be done and will split up values if it times out
        :param max_attempts: max attempts to try again
        :param split_amount: equal portions that bulk values will be split into
        :return:
        """
        try:
            sparql = SPARQLWrapper(self.endpoint_url, agent=self.user_agent)

            if bulk_values is not None:
                sparql.setQuery(query.replace("?ALL_VALUES", bulk_values))
            else:
                sparql.setQuery(query)
            # TODO adjust user agent; see https://w.wiki/CX6

            sparql.setReturnFormat(format)
            sparql.setTimeout(timeout)
            results = sparql.query().convert()
            return results

        except Exception as e:
            attempts = 1
            graph = None
            logging.info(e)
            logging.info("Query failed, sleeping for {} minutes".format(sleep_time/60))
            time.sleep(sleep_time)

            while True:

                if attempts > max_attempts:
                    logging.info("Max query attempts reached following initial query failed:")
                    logging.info(query.replace("?ALL_VALUES", bulk_values))
                    return None

                # If bulk query failed keep splitting into equal segments
                if bulk_values is not None:
                    last = False
                    bulk_split = bulk_values.split(" ")
                    ''' 
                    If after splitting the data twice it still fails it's more than likely the bug mentioned below and
                    the data needs to be queried one at a time to find the one causing the bug
                    '''
                    if attempts == (max_attempts - 1):
                        bulk_size = 1
                    else:
                        bulk_size = math.floor(total_values / split_amount)
                    if bulk_size == 0:
                        bulk_size = 1
                    for i in range(0, total_values, bulk_size):
                        bulk = ''
                        start = i
                        end = start + bulk_size
                        # If done iterating through values
                        if end >= total_values:
                            end = total_values
                            last = True
                        for idx in range(start, end):
                            bulk += bulk_split[idx] + " "
                        try:
                            sparql.setQuery(query.replace("?ALL_VALUES", bulk))

                            '''
                            Due to a very rare bug some characters cause an XML error, in this case we will get the 
                            results as a raw JSON and convert those values back into a graph
                            '''
                            if bulk_size == 1:
                                try:
                                    if graph is None:
                                        graph = sparql.query().convert()
                                    else:
                                        graph += sparql.query().convert()
                                # If .convert fails, more than likely a unicode error, get raw json and convert to graph
                                except Exception as e:
                                    sparql.setReturnFormat(JSON)
                                    new_graph = Graph()
                                    for pair in namespaces_used:
                                        new_graph.bind(pair[0], pair[1])
                                    json_results = sparql.query().convert()
                                    for json_result in json_results['results']['bindings']:
                                        s = URIRef(json_result['subject']['value'])
                                        p = URIRef(json_result['predicate']['value'])
                                        o = json_result['object']['value']
                                        if "http" in o:
                                            o = URIRef(o)
                                        else:
                                            o = Literal(o)
                                        new_graph.add((s, p, o))
                                    if graph is None:
                                        graph = new_graph
                                    else:
                                        graph += new_graph
                                    # Reset return method
                                    sparql.setReturnFormat(RDFXML)

                            else:
                                if graph is None:
                                    graph = sparql.query().convert()
                                else:
                                    graph += sparql.query().convert()

                            # If done return graph
                            if last is True:
                                return graph

                        except Exception as e:
                            # If query fails double the split amount and try again
                            attempts += 1
                            split_amount *= 2
                            logging.info(e)
                            logging.info("data had to be split {} times".format(attempts))
                            logging.info("Query failed:: {}".format(query.replace("?ALL_VALUES", bulk)))
                            time.sleep(sleep_time)
                            break


def print_and_log(message):
    logging.info(message)
    print(message)


def get_date_from_checkpoint():
    try:
        with open(os.path.join("data", "ttl", "checkpoint"), "rb") as pickle_file:
            if (input("PRODUCER: Checkpoint file found, restore checkpoint? [Y/n]")).lower() != 'n':
                checkpoint = pickle.load(pickle_file)
                last_date_string  = checkpoint['JSON_FILES'][-1].strip(".json").split('_')
                last_date = dt.datetime(year=int(last_date_string[0]),
                                        month=int(last_date_string[1]),
                                        day=int(last_date_string[2]),
                                        hour=0,
                                        minute=0,
                                        second=0,
                                        microsecond=0,
                                        tzinfo=dt.timezone.utc)
                return last_date + dt.timedelta(hours=24)
            else:
                return None

    except Exception as e:
        logging.info("No checkpoint file found")
        return None