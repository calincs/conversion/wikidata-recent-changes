import threading
import time
import logging
import random
import queue
import datetime as dt
from diff import Diff, TripleService
from helperFunctions import print_and_log


class ProducerQueue(queue.Queue):  # Inherit Queue class, make changes if needed
    @staticmethod
    def foo():
        return None


q = ProducerQueue(maxsize=7)


class ProducerThread(threading.Thread):
    def __init__(self, start_date, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None, get_diff_ids=False, resume=False):
        super(ProducerThread, self).__init__()
        self.target = target
        self.name = name
        self.diff = Diff(start_date=start_date, diff_ids=get_diff_ids, resume=resume)

    def run(self):
        print_and_log("Starting producer")
        while True:
            if not q.full():
                date_data_tup = self.diff.get_titles()
                print_and_log('Putting item in queue')
                q.put(date_data_tup)
        return


class ConsumerThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(ConsumerThread, self).__init__()
        self.target = target
        self.name = name
        self.triple = TripleService()
        self.lock = threading.Lock()
        return

    def run(self):
        logging.info("Starting consumer")
        self.triple.load_checkpoint()
        while True:
            if not q.empty() and self.lock.acquire():
                logging.debug('Consuming item in queue')
                item = q.get()
                self.triple.add_json(item)
                self.lock.release()
        return



