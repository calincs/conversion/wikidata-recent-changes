import os
import json
import datetime as dt
from dateutil.parser import parse


json_files = os.listdir('./data')
for json_file in json_files:
    with open("./data/"+json_file) as fp:
        data = json.load(fp)
        try:
            start = data.pop("START")
        except:
            start = None
        if start is not None:
            start = parse(start)
            end = start - dt.timedelta(hours=1)
            end = dt.datetime.combine(end, dt.datetime.max.time()).replace(tzinfo=dt.timezone.utc)
            data['END'] = str(end)
        data['TTL'] = 'FALSE'
        data['DATA'] = data.pop('DATA')
    with open("./data/"+json_file, "w") as fp:
        json.dump(data,fp)