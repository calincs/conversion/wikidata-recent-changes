import os

for file in os.listdir():
    if file.split(".")[-1] == "ttl":
        print("processing file")
        os.rename(file, file+".temp")
        with open(file+".temp", "r") as s:
            with open(file, "w") as d:
                while True:
                    line = s.readline()
                    if not line:
                        break
                    d.write(line.replace("[ ]", "[]"))
        os.remove(file+".temp")
