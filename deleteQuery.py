import logging
import SPARQLWrapper
import sys
import os
import json

class DeleteQuery:

    def __init__(self, max_entities=350):
        self.max_entities = max_entities
        self.endpoint_url = "http://wdqs.svc:9999/bigdata/sparql"
        self.user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])
        self.timeout = 5000
        self.sparql = SPARQLWrapper.SPARQLWrapper(self.endpoint_url, agent=self.user_agent)
        self.sparql.setCredentials(user='wikiuser', passwd="sqlpass")
        with open(os.path.join("queries", "delete_query.rq")) as fp:
            self.delete_query = fp.read()
        self.test_query = """SELECT DISTINCT ?S WHERE { ?S ?P ?O } LIMIT 500"""

    def bulk_delete(self, bulk_values):
        bulk = ''
        for uri in bulk_values:
            bulk += "wd:" + uri.split("/")[-1] + " "
        self.sparql.setQuery(self.delete_query.replace("?ALL_VALUES", bulk))
        self.sparql.setMethod(SPARQLWrapper.POST)
        self.sparql.query()

    def get_results(self, query):

        """
        Performs Sparql query using self.endpoint_url
        :param query: wikidata query
        """

        try:
            # user_agent = "WDQS-example Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
            # Learn more about user_agent; see https://w.wiki/CX6

            self.sparql.setReturnFormat(SPARQLWrapper.JSON)
            self.sparql.setTimeout(self.timeout)
            results = self.sparql.query().convert()
            if results is not None:
                return results['results']['bindings']
            else:
                return None
        except Exception as e:
            print(e)
