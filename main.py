import datetime as dt
import getopt
import sys
import logging
from helperFunctions import print_and_log, get_date_from_checkpoint
from threaded import ProducerThread, ConsumerThread

logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                    format='%(asctime)-15s (%(threadName)-9s) %(message)s', )
logging.getLogger("requests").setLevel(logging.WARNING)


def main():
    # Remove 1st argument from the list of command line arguments
    argumentList = sys.argv[1:]

    # Options
    options = "hs:dr"

    # Long options
    long_options = ["help", "start", "diff", "resume"]

    # Defaults
    get_diff_ids = False
    start_date = None
    resume = False

    try:
        # Parsing argument
        arguments, values = getopt.getopt(argumentList, options, long_options)

        # checking each argument
        for currentArgument, currentValue in arguments:

            if currentArgument in ("-h", "--help"):
                print("\t-s [YYYY-MM-DD] : Will start running thread for collecting q values and saving as JSON and a "
                      "second thread for processing JSON and getting all triples for the Q-Value and saving as ttl.")
                print('\t-d --diff : Will collect all the diff ids for each Q-Values changes and save in JSON')
                print('\t-r --resume : Will automatically resume any partially or fully completed json, will not ask to overwrite')
                exit()

            if currentArgument in ("-s", "--start"):
                print("Starting Wikidata Recent Changes Process")
                try:
                    # Assume everything is UTC 0
                    start_date = dt.datetime.strptime(currentValue, '%Y-%m-%d').replace(tzinfo=dt.timezone.utc)
                except ValueError:
                    pass

            if currentArgument in ("-d", "--diffids"):
                print("Collecting all diff ids for each Q-Value")
                get_diff_ids = True

            if currentArgument in ("-r", "--resume"):
                print("Will automatically resume any partially or fully completed json, will not ask to overwrite")
                resume = True

    except getopt.error as err:
        # output error, and return with an error code
        print_and_log(str(err))

    if start_date is None:
        # Default start date is current day
        start_date = dt.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=dt.timezone.utc)

        loaded_date = get_date_from_checkpoint()
        if loaded_date is not None:
            start_date = loaded_date
        else:
            if input("No checkpoint found or date given, use today's date? [Y/n]").lower() == 'n':
                exit()
        print_and_log("PRODUCER: Starting at date {}".format(str(start_date)))




    p = ProducerThread(name='producer', start_date=start_date, get_diff_ids=get_diff_ids, resume=resume)
    c = ConsumerThread(name='consumer')
    p.start()
    c.start()


if __name__ == "__main__":
    main()













