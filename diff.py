from helperFunctions import Query, namespaces_used, print_and_log
import logging
from dateutil.parser import parse
import json
import time
import requests
import datetime as dt
from rdflib import Graph
import os
import shutil
import pickle
from deleteQuery import DeleteQuery
import gzip
import re
import numpy as np

logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                    format='%(asctime)-15s (%(threadName)-9s) %(message)s', )
logging.getLogger("requests").setLevel(logging.WARNING)


class Diff:
    def __init__(self,
                 resume,
                 start_date,
                 diff_ids=False):
        self.checkpoints = {6: "FALSE", 12: "FALSE", 18: "FALSE"}  # Used for keeping track of json save points
        self.data_loaded = False  # True if data is loaded or if starting a new data and creating json
        # The time increment in which data change is gathered is measured in a step size of seconds
        self.default_step_size = 60
        # The step_size is dynamic but the default step size is static
        self.step_size = self.default_step_size
        self.start_dt = start_date
        self.end_dt = start_date + dt.timedelta(seconds=self.step_size)
        self.S = requests.Session()
        self.URL = "https://wikidata.org/w/api.php"
        self.PARAMS = {
            "format": "json",
            "rcdir": "newer",
            "rcnamespace": "0",
            "rcstart": self.start_dt.isoformat(),
            "rcend": self.end_dt.isoformat(),
            "rcprop": "title|ids",
            "list": "recentchanges",
            "action": "query",
            "rclimit": "500"
        }
        self.DIFF_PARAMS = {
            "format": "json",
            "fromrev": "",
            "torev": "",
            "prop": "diff|title",
            "action": "compare"
        }
        self.day_changes = {"END": "",
                            "COMPLETE": "FALSE",
                            "TTL": "FALSE",
                            "DATA": {}}
        self.get_diff_ids = diff_ids
        self.bulk_params = []
        self.all_responses = []
        with open("queries/get_bulk_triples.rq", "r") as file:
            self.bulk_query = file.read()
        self.resume = resume
        self.q = Query()
        self.filename = None

    def update_json_file(self, data_day=None):
        """
        :param data_day: the day which the data is collected
        :param complete: if True means that start time has exceeded
        :return:
        """
        if data_day is None:
            data_day = self.start_dt
        with open("data/{}_{}_{}.json".format(data_day.year,
                                              data_day.month,
                                              data_day.day), "w") as file:
            json.dump(self.day_changes, file)

    def update_day_changes_timestamp(self, read=False): #TODO can i use reset function instead?
        """
        :param read: If true you are reading start and end time from a json file
        :return:
        """
        if read is False:
            #self.day_changes["START"] = self.start_dt.isoformat()
            self.day_changes["END"] = self.end_dt.isoformat()
        else:
            # If complete is true you are on the next day so reset day_changes
            if self.day_changes["COMPLETE"] == "TRUE":
                self.day_changes["DATA"] = {}
            # start = self.day_changes.get("START")
            # if start is not None:
            #     self.start_dt = parse(start)
            end = self.day_changes.get("END")
            if end is not None:
                self.end_dt = parse(end)
            # Since loaded start -> end has completed, increment times
            self.increment_times()

    def increment_times(self, step_size=None):
        """
        Makes start time the current endtime and increments end time by current step size,
        :param step_size: default is None/default_step_size mainly used for debugging since you always increment by the
        default step size, then decrease if the query returns too many results
        :return:
        """
        # By default stepsize is always None, mainly for debugging
        if step_size is None:
            step_size = self.default_step_size
        # Set start time to end time
        self.start_dt = self.end_dt
        self.PARAMS['rcstart'] = self.start_dt.isoformat()
        # Increment end time by default step size then this will decrease if needed by self.get_changes
        self.end_dt += dt.timedelta(seconds=step_size)
        self.PARAMS['rcend'] = self.end_dt.isoformat()

    def get_changes(self):
        """
        The wikidata API returns a maximum of 500 results, if the current increment time used in rcstart and rcend
        return too many results the step size between the two is halved
        :return:
        """
        # Reset dynamic step_size
        self.step_size = self.default_step_size
        while True:
            R = self.S.get(url=self.URL, params=self.PARAMS)
            DATA = R.json()['query']['recentchanges']
            if len(DATA) < 500:
                self.increment_times()
                return DATA
            else:
                self.step_size = self.step_size >> 1
                if self.step_size == 0:
                    logging.debug("Congrats somebody made 500 changes in one second, never thought possible")
                    self.end_dt = self.start_dt + dt.timedelta(milliseconds=250)
                else:
                    self.end_dt = self.start_dt + dt.timedelta(seconds=self.step_size)
                self.PARAMS['rcend'] = self.end_dt.isoformat()

    def get_start_iso(self, arg):
        if arg == 'day':
            return parse(self.PARAMS['rcstart']).day
        elif arg == 'hour':
            return parse(self.PARAMS['rcstart']).hour
        elif arg == 'minute':
            return parse(self.PARAMS['rcstart']).minute

    def start_new_day(self, data_day):
        """
        All variables are reset and will begin on next day once get_titles is called again
        :param data_day:
        :return: day_changes temp variable so it can be put into memory of the queue
        """
        # Increment data day by 24 hours
        data_day += dt.timedelta(hours=24)
        self.start_dt = data_day
        self.step_size = self.default_step_size
        self.end_dt = data_day + dt.timedelta(seconds=self.step_size)
        # Clear DATA and update timestamp
        temp_day_changes = self.day_changes.copy()
        self.day_changes["DATA"] = {}
        self.day_changes["COMPLETE"] = "FALSE"
        self.update_day_changes_timestamp()
        self.data_loaded = False
        for key in self.checkpoints:
            self.checkpoints[key] = "FALSE"

        return temp_day_changes

    def get_titles(self):
        data_day = self.start_dt               # Datetime object for tracking current day that always stays at 00:00:00

        while True:

            if self.data_loaded is False:
                self.data_loaded = True
                try:
                    self.filename = "{}_{}_{}.json".format(self.start_dt.year, self.start_dt.month, self.start_dt.day)
                    with open('data/{}'.format(self.filename)) as file:
                        string = "Existing titles data found for {}-{}-{}, replace y|[N]".format(self.start_dt.year,
                                                                                                 self.start_dt.month,
                                                                                                 self.start_dt.day)
                        if self.resume or ((input(string)).lower() != "y"):
                            self.day_changes = json.load(file)
                            if self.day_changes["COMPLETE"] == "TRUE":
                                return tuple((self.filename, self.start_new_day(data_day)))
                            else:
                                self.update_day_changes_timestamp(read=True)            # Read start time from json file
                            if self.end_dt.hour in self.checkpoints:
                                self.checkpoints[self.start_dt.hour] = "TRUE"
                            data_day = dt.datetime.combine(self.start_dt,
                                                           dt.datetime.min.time()).replace(tzinfo=dt.timezone.utc)
                        else:
                            self.update_json_file()
                except json.JSONDecodeError as e:
                    print_and_log("JSON failed to load, resetting: {}".format(e))
                except Exception as e:
                    print_and_log("Starting new JSON for {}-{}-{}".format(self.start_dt.year,
                                                                              self.start_dt.month,
                                                                              self.start_dt.day))

            # Wait one hour till after time has passed
            elif (self.start_dt + dt.timedelta(minutes=5)) < dt.datetime.utcnow().replace(tzinfo=dt.timezone.utc):
                # If start_dt day is equal to the max time of dataday, it is complete
                if (self.start_dt == dt.datetime.combine(data_day,
                                                         dt.datetime.max.time()).replace(tzinfo=dt.timezone.utc)):
                    # Return days data to Queue
                    self.day_changes["COMPLETE"] = "TRUE"
                    self.update_json_file(data_day=data_day)
                    return tuple((self.filename, self.start_new_day(data_day)))

                else:
                    # Otherwise get changes
                    # If end datetime not the same day, set it to the max of the dataday
                    if self.end_dt.day != data_day.day:
                        self.end_dt = dt.datetime.combine(data_day,
                                                          dt.datetime.max.time()).replace(tzinfo=dt.timezone.utc)
                    # Update day_changes timestamp before running so that day_changes START and END match the most
                    # recently collected data.
                    self.update_day_changes_timestamp()
                    # Get the changes and increment start_dt and end_dt
                    RECENTCHANGES = self.get_changes()
                    for rc in RECENTCHANGES:
                        rc_type = rc.get('type')
                        # If recent change is new or an edit add it to our list of titles/qvalues
                        if rc_type in ('edit', 'new'):
                            title = rc.get('title')
                            if self.day_changes['DATA'].get(title) is None:
                                self.day_changes['DATA'][title] = []

                            # If change was an edit get the from and to id and differences if wanted
                            if rc_type == 'edit':
                                if self.get_diff_ids is True:
                                    from_id = rc['old_revid']
                                    to_id = rc['revid']
                                    self.day_changes['DATA'][title].append({"from_id": from_id, "to_id": to_id})

                    # If processed six hours worth of titles backup, saving JSON takes alot of time
                    hour = self.start_dt.hour
                    if hour in self.checkpoints:
                        if self.checkpoints.get(hour) == 'FALSE':
                            self.checkpoints[hour] = 'TRUE'
                            self.update_json_file()

            else:
                # Sleep for 5 min
                time.sleep(300)


class TripleService:
    def __init__(self, max_days=7):
        self.delete_size = 500
        self.q_values = set()
        self.start_date = None
        self.count = 0                        # Count of JSON files loaded
        self.file_counter = 1                 # Count of .ttl files dumped
        self.max_days = max_days
        self.info_json = None
        self.graph = None
        self.reset_graph()
        self.checkpoint = {}
        self.load_data_command = """./loadData.sh -n wdq -d /wdqs/data/ttl"""
        # Only used if loading from checkpoint
        self.completed_files = {'JSON_FILES': []}
        self.data_path = "/wdqs/data/ttl/"
        try:
            with open("completed_files.json") as fp:
                self.completed_files = json.load(fp)
        except:
            pass
        try:
            os.mkdir("/wdqs/data/ttl")
            os.mkdir("/wdqs/data/ttl/archive")
        except:
            pass

    def reset_class(self):
        self.q_values = set()
        self.start_date = None
        self.info_json = {}
        self.count = 0                        # Count of JSON files loaded
        self.file_counter = 1

    def load_checkpoint(self):
        try:
            with open(os.path.join("data", "ttl", "checkpoint"), "rb") as pickle_file:
                if (input("CONSUMER: Checkpoint file found, restore checkpoint? [Y/n]")).lower() != 'n':
                    self.checkpoint = pickle.load(pickle_file)
                    self.file_counter = self.checkpoint['FILE_COUNTER']
                    self.q_values = self.checkpoint['VALUES_REMAINING']
                    self.set_info_json()
                    self.info_json['JSON_FILES'] = self.checkpoint['JSON_FILES']
                    self.reset_graph()
                    self.get_triples()
                else:
                    pass

        except:
            print_and_log("No checkpoint file found")

    def save_checkpoint(self, data):
        with open(os.path.join("data", "ttl", "checkpoint"), "wb") as pickle_file:
            pickle.dump(data, pickle_file)

    def reset_graph(self):
        self.graph = Graph()
        # https://www.mediawiki.org/wiki/Wikibase/Indexing/RDF_Dump_Format#Prefixes_used
        for pair in namespaces_used:
            self.graph.bind(pair[0], pair[1])

    def set_info_json(self):
        self.info_json = {'COMPLETE': 'FALSE',
                          'JSON_FILES': []}

    def add_json(self, date_data_tuple):
        if self.count == 0:
            self.set_info_json()
        # Union all q_values together
        self.q_values = self.q_values.union(set(i for i in date_data_tuple[1]['DATA'].keys()))
        self.info_json['JSON_FILES'].append(date_data_tuple[0])
        self.count += 1
        if self.count >= self.max_days:
            self.get_triples()
            self.count = 0
        return

    # def reset_info_json(self):
    #     """
    #     Resets the info json and archives the json files containing the q-values that have changes
    #     :return: None
    #     """
    #
    #     # Update completed files and move file to archive and clear json
    #     for file in self.info_json.get("JSON_FILES"):
    #         self.completed_files['JSON_FILES'].append(file)
    #         source_file = os.path.join(os.getcwd(), "data", file)
    #         dest_file = os.path.join(os.getcwd(), "data", "archive", file)
    #         shutil.copy(source_file, dest_file)
    #         os.system("gzip {}".format(dest_file))
    #     self.set_info_json()

    def get_triples(self, max_graph_size=1000000):
        start_time = time.time()
        counter = 0                           # Used for keeping stack of bulk values string of wd:<Q-VALUES>
        bulk_values = ''

        # Get query template and initialize query_api class
        with open("queries/get_bulk_triples.rq", "r") as fp:
            query = fp.read()
        query_q = Query()
        delete_q = DeleteQuery()

        self.q_values = list(self.q_values)

        # Delete all triples associated with the Q-Values
        print_and_log("Deleting old q values")
        delete_set = set()
        for q_value in self.q_values:
            delete_set.add(q_value)
            if len(delete_set) > self.delete_size:
                delete_q.bulk_delete(delete_set)
                delete_set = set()
        delete_q.bulk_delete(delete_set)

        # Log ttl json info
        json_file = "data/ttl/wikidump-{:09d}.json".format(self.file_counter)
        with open(json_file, "w") as info_json_fp:
            json.dump(self.info_json, info_json_fp)

        while len(self.q_values) > 0:
            value = self.q_values.pop()
            counter += 1
            bulk_values += "wd:" + value + " "
            if (counter == query_q.max_query) or (len(self.q_values) == 0):
                # Query Wikidata
                results = query_q.get_results(query=query, bulk_values=bulk_values, total_values=counter)
                if (type(results) == list) or (results is None):
                    print_and_log("Query failed after attempts to fix")
                else:
                    # Append results to graph
                    self.graph += results

                if (len(self.graph) > max_graph_size) or (len(self.q_values) == 0):
                    # Serialize Graph
                    print_and_log("Serializing /wdqs/data/ttl/wikidump-{:09d}.ttl".format(self.file_counter))
                    self.graph.serialize(destination=self.data_path + "unfixed_wikidump-{:09d}.ttl".format(self.file_counter),
                                         format='turtle', )

                    '''
                    Currently the blazegraph scripts depend on libraries that cannot parse a blank nodes in the form
                    of [ ], for now we will just deal with this manually via line.replace("[ ]", [])  There is also a 
                    bug where scientific notation numbers are getting cast to scientific notation, which blazegraph cannot
                    seem to be able to read, hence we need to convert to a float64
                    '''
                    with open(self.data_path + "unfixed_wikidump-{:09d}.ttl".format(self.file_counter), 'r') as s:
                        with open(self.data_path + "wikidump-{:09d}.ttl".format(self.file_counter), 'w') as d:
                            while True:
                                line = s.readline()
                                if not line:
                                    break
                                # Fixes bug where blank nodes are read incorrectly
                                line = line.replace("[ ]", "[]")
                                matched = re.findall(
                                    "[ ]+([-]?[0-9][.]?([0-9]+)?[eE][-]?[0-9]+[.]?[0-9]?)+[ ]?[\.,;]{1}$", line)
                                if len(matched) > 0:

                                    # Numpy does not like scientific notation ending in .0
                                    if matched[0][0][-2:] == '.0':
                                        final_match = matched[0][0][:-2]
                                    else:
                                        final_match = matched[0][0]

                                    # Chose an arbitrary long float decimal, TODO make this better?
                                    line = line.replace(matched[0][0], "{:.50f}".format(np.float64(final_match)))
                                d.write(line)
                    os.remove(self.data_path + "unfixed_wikidump-{:09d}.ttl".format(self.file_counter))

                    # Gzip File
                    print_and_log("Gzipping data")
                    with open(self.data_path + "wikidump-{:09d}.ttl".format(self.file_counter), 'rb') as f_in:
                        with gzip.open(self.data_path + "wikidump-{:09d}.ttl.gz".format(self.file_counter), "wb") as f_out:
                            shutil.copyfileobj(f_in, f_out)
                    os.remove(self.data_path + "wikidump-{:09d}.ttl".format(self.file_counter))

                    # Reset graph and increment counter
                    self.file_counter += 1
                    self.reset_graph()

                    # Update JSON info & checkpoint
                    self.save_checkpoint({**{"VALUES_REMAINING": self.q_values,
                                             "FILE_COUNTER": self.file_counter},
                                          **self.info_json})

                bulk_values = ''
                counter = 0

        # Upload data and reset counters
        print_and_log("Uploading data via wdqs ./loadData script")
        error_no = os.system(self.load_data_command)
        if error_no == 0:
            self.reset_class()
            print_and_log("Deleting old archived .ttl files")
            for file in os.listdir(self.data_path+"archive"):
                if file.split(".")[-1] == "gz":
                    os.remove(self.data_path+"archive/" + file)
            print_and_log("Movings .ttl files to archive")
            for file in os.listdir(self.data_path):
                if file.split(".")[-1] == "gz":
                    shutil.move(self.data_path + file, self.data_path+"archive/" + file)
        else:
            print_and_log("Loading data failed")

        # Set info json to complete, then reset
        self.info_json["COMPLETE"] = "TRUE"
        with open(json_file, "w") as info_json_fp:
            json.dump(self.info_json, info_json_fp)

        # reset Variables
        self.reset_class()
        self.set_info_json()
        self.reset_graph()
        self.q_values = set()
        print("{} days of data took : {:.1f} minutes".format(self.max_days, (time.time() - start_time) / 60))
