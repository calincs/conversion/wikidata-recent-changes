#!/usr/bin/env bash

docker-compose down
docker volume rm docker_mediawiki-images-data docker_mediawiki-mysql-data docker_quickstatements-data docker_recent-changes
docker-compose build --no-cache
docker-compose up --force-recreate -d
