import SPARQLWrapper as sw
from rdflib import Graph

sparql = sw.SPARQLWrapper("https://query.wikidata.org/sparql")

sparql.setQuery("""
CONSTRUCT {
    ?s ?o ?p
} WHERE {
    VALUES ?s { wd:Q64019250 wd:Q36322 wd:Q170583}
    ?s ?o ?p .
}
""")
sparql.setReturnFormat(sw.RDFXML)
results = sparql.query().convert()
results.serialize(destination="output.txt", format='turtle')
