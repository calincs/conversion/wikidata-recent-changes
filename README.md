# Wikidata Recent Changes

Exports Wikidata recent changes using Wikidata API

Launching In Docker
-------------------

The start date must be defined in the docker-compose file (Line 168)
From docker folder run docker-compose up-d.  If you would like to recreate everything run the ./rebuildDocker.sh script.

Launching Manually
-------------------

Call python3 main.py -s 2021-##-## -d -r

-s date will be the latest <date>.json file that exists 
-d will gather diff ids for each q value
-r will resume a file if it exists.

How It Works
------------

This process has two threads the producer (that which creates data) and the consumer (that which processes the data).  
Each can hold exactly 7 days of recent changes information.  The producer and consumer classes are found in threaded.py.

The producer starts at a date and queries wikidata for every 30 seconds all the changes have been made.  The Wikidata
API has a limit to how many entities it can return, if this limit is met 30 seconds will be recursively decreased until
the API returns less than the limit.

Once a days worth of data is processed, the information is given to the consumer, this data is passed into the Diff class
where 7 days worth of Q-Values are put into a set. These Q-Values are then deleted from the blazegraph and then all data 
for these Q-Values are queried and put into a new ttl file.  This turtle is then loaded into the blazegraph using the 
wdqs script.

Known Bugs
----------

There is a known bug on how RDFLib handles numbers in scientific notation, there is a fix for this (Line 417 diff.py).
Eventually this fix will be pushed by the RDFLib maintainers but it will be a very long time before you see this latest 
version pushed to pypi.  You could use RDFLib from source if wanted.  More info on the bug can be found 
[here](https://github.com/RDFLib/rdflib/issues/1314#issuecomment-844369051).